/* Lesson 1

interface vehicle {
    Model: string
    PurchasePrice: number
    LotPrice: number
    isInspected: boolean
}

function isProfitable(car: vehicle): boolean {
    return car.PurchasePrice < car.LotPrice
}

function isSellable(car: vehicle): boolean {
    return isProfitable(car) && car.isInspected
}

function createLabel(car: vehicle): void {
    return isSellable(car) ? drawPositiveTextOnScreen(car.Model) : drawNegativeTextOnScreen(car.Model)
}

function drawPositiveTextOnScreen(carModel: string): void {
    console.log("Yea sure, we can sell the " + carModel)
}

function drawNegativeTextOnScreen(carModel: string): void {
    console.log("Not happening lol, that " + carModel + " isnt worth our time yet")
}

let carlist = new Array

carlist.push(
    { Model: "Ford", PurchasePrice: 25000, LotPrice: 6000, isInspected: true },
    { Model: "Chevy", PurchasePrice: 23000, LotPrice: 9000, isInspected: false },
    { Model: "Toyota", PurchasePrice: 21000, LotPrice: 12000, isInspected: true },
    { Model: "Honda", PurchasePrice: 19000, LotPrice: 15000, isInspected: false },
    { Model: "Tesla", PurchasePrice: 17000, LotPrice: 18000, isInspected: true },
    { Model: "Lambo", PurchasePrice: 15000, LotPrice: 21000, isInspected: false },
    { Model: "Porsche", PurchasePrice: 13000, LotPrice: 24000, isInspected: true },
    { Model: "BMW", PurchasePrice: 11000, LotPrice: 27000, isInspected: false },
)

for (let index = 0; index < carlist.length; index++) {
    createLabel(carlist[index])
} 

function failure(maxFibi: number) {
    let baseFibi = 0
    let newFibi = 1
    let oldFibi = 0
    
    while (maxFibi > newFibi) {
        console.log(baseFibi)
        baseFibi = newFibi + oldFibi
        oldFibi = newFibi
        newFibi = baseFibi
    }
}

failure(999)
*/
function getRandomInt(min: number, max: number) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min) + min); //The maximum is exclusive and the minimum is inclusive
}

interface vehicle {
    Model: string
    PurchasePrice: number
    LotPrice: number
    isInspected: boolean
}

enum ModelType {
    AC_3000ME = 0,
    ALFA_ROMEO_4C = 1,
    ALPINE_A108 = 2,
    ARIEL_ATOM = 3,
    ARRINERA_HUSSARYA = 4,
    ASCARI_A10 = 5,
    ASTON_MARTIN_VANTAGE = 6,
    AUDI_TT = 7,
    BENTLEY_CONTINENTAL_GT = 8,
    BUICK_REATTA = 9,
    BUGATTI_EB110 = 10,
    BMW_Z8 = 11,
    CAPARO_T1 = 12,
    CATERHAM_21 = 13,
    CHEVROLET_CAMARO = 14,
    DODGE_CHALLENGER = 15,
    DODGE_VIPER = 16,
    FERRARI_458_ITALIA = 17,
    FIAT_X19 = 18,
    FORD_MUSTANG = 19,
    HONDA_INTEGRA = 20,
    HONDA_NSX = 21,
    HYUNDAI_GENESIS_COUPE = 22,
    HYUNDAI_TIBURON = 23,
    IFR_ASPID = 24,
    INFINITI_EMERG_E = 25,
    ISUZU_117_COUPE = 26,
    JAGUAR_F_TYPE = 27,
    LAMBORGHINI_MURCIELAGO = 28,
    LEXUS_LFA = 29,
    LOTUS_ELISE = 30,
    MARUSSIA_B1 = 31,
    MAZDA_RX_8 = 32,
    MASTRETTA_MXT = 33,
    MCLAREN_12C = 34,
    NISSAN_240SX = 35,
    OPEL_TIGRA = 36,
    PONTIAC_GTO = 37,
    PORSCHE_997 = 38,
    RENAULT_WIND = 39,
    SATURN_SKY = 40,
    TOYOTA_86 = 41,
}

let carlist = new Array<vehicle>()

function createVehicle(numberOfCars: number) {
    while (numberOfCars > 0) {
        let modelRand = getRandomInt(0, 42)
        let purchaseRand = getRandomInt(1000, 999999)
        let lotRand = getRandomInt(1000, 999999)
        let inspectedRand = Math.random() < 0.5
        carlist.push({ Model: ModelType[modelRand], PurchasePrice: purchaseRand, LotPrice: lotRand, isInspected: inspectedRand })
        numberOfCars--
    }
}

function isProfitable(car: vehicle): boolean {
    return car.PurchasePrice < car.LotPrice
}

function isSellable(car: vehicle): boolean {
    return isProfitable(car) && car.isInspected
}

function createLabel(car: vehicle): void {
    return isSellable(car) ? drawPositiveTextOnScreen(car.Model) : drawNegativeTextOnScreen(car.Model)
}

function drawPositiveTextOnScreen(carModel: string): void {
    console.log("Yea sure, we can sell the " + carModel)
}

function drawNegativeTextOnScreen(carModel: string): void {
    console.log("Not happening lol, that " + carModel + " isnt worth our time yet")
}

createVehicle(100)

for (let index = 0; index < carlist.length; index++) {
    createLabel(carlist[index])
} 
